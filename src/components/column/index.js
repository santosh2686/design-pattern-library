import React, { memo } from 'react'
import {
  string, shape, number,
  oneOfType, element, arrayOf,
} from 'prop-types'

import { classNames } from '../../util'

import './_style.scss'

const blk = 'dls-grid-column'

const Column = ({
  tag: Tag,
  children,
  col,
  offset,
  className,
  dataAutoId,
}) => {
  const {
    xxlg, xlg, lg, md, sm, xs,
  } = col
  const {
    xxlg: offsetXxlg,
    xlg: offsetXlg,
    lg: offsetLg,
    md: offsetMd,
    sm: offsetSm,
    xs: offsetXs,
  } = offset

  const eltClass = classNames({
    blk,
    className,
    mods: {
      [`xs-${xs}`]: !!xs,
      [`sm-${sm}`]: !!sm,
      [`md-${md}`]: !!md,
      [`lg-${lg}`]: !!lg,
      [`xlg-${xlg}`]: !!xlg,
      [`xxlg-${xxlg}`]: !!xxlg,

      [`offset-xs-${offsetXs}`]: !!offsetXs,
      [`offset-md-${offsetMd}`]: !!offsetMd,
      [`offset-sm-${offsetSm}`]: !!offsetSm,
      [`offset-lg-${offsetLg}`]: !!offsetLg,
      [`offset-xlg-${offsetXlg}`]: !!offsetXlg,
      [`offset-xxlg-${offsetXxlg}`]: !!offsetXxlg,
    },
  })

  return (
    <Tag className={eltClass} data-auto-id={dataAutoId}>
      {children}
    </Tag>
  )
}

Column.propTypes = {
  children: oneOfType([
    element,
    arrayOf(element),
  ]).isRequired,
  col: shape({
    xs: number,
  }).isRequired,
  offset: shape({
    xs: number,
  }),
  className: string,
  dataAutoId: string,
  tag: string,
}

Column.defaultProps = {
  offset: {},
  className: '',
  dataAutoId: 'column',
  tag: 'div',
}

export default memo(Column)
