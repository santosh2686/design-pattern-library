import classNames from './index'

describe('util > classNames', () => {
  it('should return the formatted class name', () => {
    const result = classNames({
      blk: 'my-class'
    })
    expect(result).toEqual('my-class')
  })
  it('should return the formatted class name with modifier', () => {
    const result = classNames({
      blk: 'my-class',
      mods: {
        'my-mod': true
      }
    })
    expect(result).toEqual('my-class my-class--my-mod')
  })
  it('should return the formatted class name for an element', () => {
    const result = classNames({
      blk: 'my-class',
      elt: 'my-element'
    })
    expect(result).toEqual('my-class__my-element')
  })

  it('should return the formatted class name for an element with modifier', () => {
    const result = classNames({
      blk: 'my-class',
      elt: 'my-element',
      mods: {
        'my-mod': true
      }
    })
    expect(result).toEqual('my-class__my-element my-class__my-element--my-mod')
  })

  fit('should return the formatted class name with new custom class', () => {
    const result = classNames({
      blk: 'my-class',
      className: 'custom-class'
    })
    expect(result).toEqual('my-class custom-class')
  })

})
