"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = require("prop-types");

var _util = require("../../util");

var _headerCell = _interopRequireDefault(require("./header-cell"));

var _bodyCell = _interopRequireDefault(require("./body-cell"));

require("./_style.scss");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var blk = 'dls-table';

var Table = function Table(_ref) {
  var dataAutoId = _ref.dataAutoId,
      className = _ref.className,
      columns = _ref.columns,
      data = _ref.data,
      noRecordMessage = _ref.noRecordMessage;
  var eltClass = (0, _util.classNames)({
    blk: blk,
    className: className
  });
  var headerClass = (0, _util.classNames)({
    blk: blk,
    elt: 'header'
  });
  var bodyClass = (0, _util.classNames)({
    blk: blk,
    elt: 'body'
  });
  return /*#__PURE__*/_react["default"].createElement("table", {
    className: eltClass,
    "data-auto-id": dataAutoId
  }, /*#__PURE__*/_react["default"].createElement("thead", {
    className: headerClass
  }, /*#__PURE__*/_react["default"].createElement("tr", null, columns.map(function (column, index) {
    var title = column.title,
        columnClassName = column.className;
    var uniqueHeaderColumn = "header_column_".concat(index);
    return /*#__PURE__*/_react["default"].createElement(_headerCell["default"], {
      key: uniqueHeaderColumn,
      title: title,
      className: columnClassName
    });
  }))), /*#__PURE__*/_react["default"].createElement("tbody", {
    className: bodyClass
  }, data.length === 0 && /*#__PURE__*/_react["default"].createElement("tr", null, /*#__PURE__*/_react["default"].createElement("td", {
    colSpan: columns.length
  }, noRecordMessage)), data.map(function (dataItem, rowIndex) {
    var uniqueRow = "body_row_".concat(rowIndex);
    return /*#__PURE__*/_react["default"].createElement("tr", {
      key: uniqueRow
    }, columns.map(function (column, columnIndex) {
      var uniqueRowColumn = "body_row_".concat(rowIndex, "_column_").concat(columnIndex);
      return /*#__PURE__*/_react["default"].createElement(_bodyCell["default"], {
        key: uniqueRowColumn,
        column: column,
        dataItem: dataItem
      });
    }));
  })));
};

Table.propTypes = {
  className: _propTypes.string,
  dataAutoId: _propTypes.string,
  columns: (0, _propTypes.arrayOf)((0, _propTypes.shape)({
    title: _propTypes.node,
    map: _propTypes.string
  })).isRequired,
  data: (0, _propTypes.arrayOf)((0, _propTypes.shape)({
    name: _propTypes.string
  })),
  noRecordMessage: _propTypes.node
};
Table.defaultProps = {
  className: '',
  dataAutoId: 'table',
  data: [],
  noRecordMessage: 'No Record Found.'
};

var _default = /*#__PURE__*/(0, _react.memo)(Table);

exports["default"] = _default;
//# sourceMappingURL=index.js.map