import React, { memo } from 'react'
import {
  arrayOf, node, string, shape,
} from 'prop-types'

import { classNames } from '../../util'

import HeaderCell from './header-cell'
import BodyCell from './body-cell'

import './_style.scss'

const blk = 'dls-table'

const Table = ({
  dataAutoId,
  className,
  columns,
  data,
  noRecordMessage,
}) => {
  const eltClass = classNames({
    blk,
    className,
  })

  const headerClass = classNames({
    blk,
    elt: 'header',
  })

  const bodyClass = classNames({
    blk,
    elt: 'body',
  })

  return (
    <table
      className={eltClass}
      data-auto-id={dataAutoId}
    >
      <thead className={headerClass}>
        <tr>
          {columns.map((column, index) => {
            const { title, className: columnClassName } = column
            const uniqueHeaderColumn = `header_column_${index}`
            return (
              <HeaderCell
                key={uniqueHeaderColumn}
                title={title}
                className={columnClassName}
              />
            )
          })}
        </tr>
      </thead>
      <tbody className={bodyClass}>
        {data.length === 0 && (
          <tr>
            <td colSpan={columns.length}>
              {noRecordMessage}
            </td>
          </tr>
        )}
        {data.map((dataItem, rowIndex) => {
          const uniqueRow = `body_row_${rowIndex}`
          return (
            <tr key={uniqueRow}>
              {columns.map((column, columnIndex) => {
                const uniqueRowColumn = `body_row_${rowIndex}_column_${columnIndex}`
                return (
                  <BodyCell
                    key={uniqueRowColumn}
                    column={column}
                    dataItem={dataItem}
                  />
                )
              })}
            </tr>
          )
        })}
      </tbody>
    </table>
  )
}

Table.propTypes = {
  className: string,
  dataAutoId: string,
  columns: arrayOf(shape({
    title: node,
    map: string,
  })).isRequired,
  data: arrayOf(shape({
    name: string,
  })),
  noRecordMessage: node,
}

Table.defaultProps = {
  className: '',
  dataAutoId: 'table',
  data: [],
  noRecordMessage: 'No Record Found.',
}

export default memo(Table)
