"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = require("prop-types");

var _util = require("../../util");

var _tabButton = _interopRequireDefault(require("../tab-button"));

require("./_style.scss");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var blk = 'dls-tab-list';

var TabList = function TabList(_ref) {
  var dataAutoId = _ref.dataAutoId,
      className = _ref.className,
      data = _ref.data,
      fluid = _ref.fluid,
      selected = _ref.selected,
      isRouted = _ref.isRouted,
      _selectHandler = _ref.selectHandler;
  var eltClass = (0, _util.classNames)({
    blk: blk,
    className: className
  });
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: eltClass,
    "data-auto-id": dataAutoId,
    role: "tablist"
  }, data.map(function (_ref2, index) {
    var label = _ref2.label,
        link = _ref2.link,
        icon = _ref2.icon;
    var key = "tab-".concat(index);
    return /*#__PURE__*/_react["default"].createElement(_tabButton["default"], {
      key: key,
      isRouted: isRouted,
      label: label,
      dataAutoId: key,
      icon: icon,
      selected: selected === index,
      link: link,
      fluid: fluid,
      selectHandler: function selectHandler() {
        return _selectHandler(index);
      }
    });
  }));
};

TabList.propTypes = {
  data: (0, _propTypes.arrayOf)((0, _propTypes.shape)({
    label: (0, _propTypes.oneOfType)([_propTypes.node, _propTypes.func]).isRequired,
    link: _propTypes.string
  })).isRequired,
  selected: _propTypes.number,
  isRouted: _propTypes.bool,
  fluid: _propTypes.bool,
  selectHandler: _propTypes.func,
  className: _propTypes.string,
  dataAutoId: _propTypes.string
};
TabList.defaultProps = {
  selected: 0,
  isRouted: false,
  fluid: false,
  selectHandler: function selectHandler() {},
  className: '',
  dataAutoId: 'tabs'
};

var _default = /*#__PURE__*/(0, _react.memo)(TabList);

exports["default"] = _default;
//# sourceMappingURL=index.js.map