import React, { memo } from 'react'
import { NavLink } from 'react-router-dom'
import {
  bool, func, string, oneOf, node, shape,
} from 'prop-types'

import { classNames } from '../../util'

import './_style.scss'

const blk = 'dls-anchor'

const Anchor = ({
  category,
  asButton,
  asLink,
  type,
  href,
  target,
  align,
  disabled,
  standalone,
  clickHandler,
  dataAutoId,
  className,
  children,
  attributes,
}) => {
  const eltClass = classNames({
    blk,
    className,
    mods: {
      [category]: !!category,
      [`${category}-disabled`]: disabled,
      standalone,
      [`align-${align}`]: !!align,
      button: asButton,
    },
  })

  if (asButton) {
    return (
      <button
        // eslint-disable-next-line react/button-has-type
        type={type}
        data-auto-id={dataAutoId}
        className={eltClass}
        onClick={clickHandler}
        disabled={disabled}
        aria-disabled={disabled}
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...attributes}
      >
        {children}
      </button>
    )
  }

  if (asLink) {
    return (
      <NavLink
        to={href}
        className={eltClass}
        data-auto-id={dataAutoId}
        onClick={clickHandler}
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...attributes}
      >
        {children}
      </NavLink>
    )
  }

  return (
    <a
      data-auto-id={dataAutoId}
      href={href}
      target={target}
      className={eltClass}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...attributes}
    >
      {children}
    </a>
  )
}

Anchor.propTypes = {
  category: oneOf(['primary', 'secondary']),
  type: oneOf(['button', 'submit', 'reset']),
  align: oneOf(['left', 'center', 'right']),
  asButton: bool,
  asLink: bool,
  href: string,
  target: string,
  disabled: bool,
  standalone: bool,
  dataAutoId: string,
  className: string,
  children: node.isRequired,
  clickHandler: func,
  attributes: shape({}),
}

Anchor.defaultProps = {
  category: 'primary',
  align: 'center',
  type: 'button',
  asButton: false,
  asLink: false,
  href: '/',
  target: '_self',
  disabled: false,
  standalone: false,
  dataAutoId: '',
  className: '',
  clickHandler: () => {},
  attributes: {},
}

export default memo(Anchor)
