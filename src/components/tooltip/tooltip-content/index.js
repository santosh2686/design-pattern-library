import React, { PureComponent } from 'react'
import {
  bool, func, node, string,
} from 'prop-types'

import { classNames } from '../../../util'

import CloseButton from '../../close-button'

import './_style.scss'

const blk = 'dls-tooltip-content'

class TooltipContent extends PureComponent {
  render() {
    const {
      className, category, children, isClosable, closeButtonDataAutoId, closeHandler,
    } = this.props
    const eltClass = classNames({
      blk,
      mods: {
        [category]: !!category,
      },
      className,
    })

    const contentClass = classNames({
      blk,
      elt: 'content',
    })

    const closeBtnClass = classNames({
      blk,
      elt: 'close',
      mods: {
        [category]: !!category,
      },
    })

    return (
      <div className={eltClass}>
        <div className={contentClass}>
          {children}
        </div>
        {isClosable && (
          <CloseButton
            dataAutoId={closeButtonDataAutoId}
            clickHandler={closeHandler}
            className={closeBtnClass}
          />
        )}
      </div>
    )
  }
}

TooltipContent.propTypes = {
  children: node.isRequired,
  className: string,
  closeHandler: func,
  closeButtonDataAutoId: string,
  category: string.isRequired,
  isClosable: bool,
}

TooltipContent.defaultProps = {
  className: '',
  closeHandler: () => {},
  closeButtonDataAutoId: 'btnClose',
  isClosable: false,
}

export default TooltipContent
