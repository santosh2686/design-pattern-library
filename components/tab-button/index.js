"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactRouterDom = require("react-router-dom");

var _propTypes = require("prop-types");

var _util = require("../../util");

require("./_style.scss");

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var blk = 'dls-tab-button';

var TabButton = function TabButton(_ref) {
  var className = _ref.className,
      label = _ref.label,
      link = _ref.link,
      fluid = _ref.fluid,
      selected = _ref.selected,
      isRouted = _ref.isRouted,
      selectHandler = _ref.selectHandler;
  var linkEltClass = (0, _util.classNames)({
    blk: blk,
    className: className,
    mods: {
      fluid: fluid
    }
  });
  var tabLabel = label instanceof Function ? label() : label;

  if (isRouted) {
    return /*#__PURE__*/_react["default"].createElement(_reactRouterDom.NavLink, {
      to: link,
      exact: true,
      className: linkEltClass,
      activeClassName: "dls-tab-button--selected"
    }, tabLabel);
  }

  var buttonEltClass = (0, _util.classNames)({
    blk: blk,
    className: className,
    mods: {
      fluid: fluid,
      selected: selected
    }
  });
  return /*#__PURE__*/_react["default"].createElement("button", {
    type: "button",
    onClick: selectHandler,
    className: buttonEltClass,
    role: "tab",
    "aria-selected": selected
  }, tabLabel);
};

TabButton.propTypes = {
  className: _propTypes.string,
  label: (0, _propTypes.oneOfType)([_propTypes.node, _propTypes.func]).isRequired,
  link: _propTypes.string,
  fluid: _propTypes.bool,
  selected: _propTypes.bool,
  isRouted: _propTypes.bool,
  selectHandler: _propTypes.func
};
TabButton.defaultProps = {
  className: '',
  link: '/',
  fluid: true,
  selected: false,
  isRouted: false,
  selectHandler: function selectHandler() {}
};

var _default = /*#__PURE__*/(0, _react.memo)(TabButton);

exports["default"] = _default;
//# sourceMappingURL=index.js.map