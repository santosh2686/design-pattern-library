"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "ModalHeader", {
  enumerable: true,
  get: function get() {
    return _modalHeader["default"];
  }
});
Object.defineProperty(exports, "ModalBody", {
  enumerable: true,
  get: function get() {
    return _modalBody["default"];
  }
});
Object.defineProperty(exports, "ModalFooter", {
  enumerable: true,
  get: function get() {
    return _modalFooter["default"];
  }
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = require("prop-types");

var _util = require("../../util");

var _modalHeader = _interopRequireDefault(require("./modal-header"));

var _modalBody = _interopRequireDefault(require("./modal-body"));

var _modalFooter = _interopRequireDefault(require("./modal-footer"));

require("./_style.scss");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var blk = 'dls-modal';

var Modal = function Modal(_ref) {
  var children = _ref.children,
      className = _ref.className;
  var containerClass = (0, _util.classNames)({
    blk: blk,
    elt: 'container',
    className: className
  });
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: blk
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: containerClass
  }, children));
};

Modal.propTypes = {
  children: _propTypes.node,
  className: _propTypes.string
};
Modal.defaultProps = {
  children: '',
  className: ''
};
var ModalWithMemo = /*#__PURE__*/(0, _react.memo)(Modal);
exports["default"] = ModalWithMemo;
//# sourceMappingURL=index.js.map