"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var isEmpty = function isEmpty(value) {
  return value === undefined || value === null || value === '';
};

var _default = isEmpty;
exports["default"] = _default;
//# sourceMappingURL=index.js.map