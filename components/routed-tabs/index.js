"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactRouterDom = require("react-router-dom");

var _propTypes = require("prop-types");

var _util = require("../../util");

var _tabList = _interopRequireDefault(require("../tab-list"));

require("./_style.scss");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var blk = 'dls-routed-tabs';

var RoutedTabs = function RoutedTabs(_ref) {
  var dataAutoId = _ref.dataAutoId,
      data = _ref.data,
      fluid = _ref.fluid,
      defaultSelected = _ref.defaultSelected,
      className = _ref.className,
      contentClassName = _ref.contentClassName;
  var contentClass = (0, _util.classNames)({
    blk: blk,
    elt: 'content',
    className: contentClassName
  });
  return /*#__PURE__*/_react["default"].createElement(_react["default"].Fragment, null, /*#__PURE__*/_react["default"].createElement(_tabList["default"], {
    isRouted: true,
    data: data,
    fluid: fluid,
    className: className,
    dataAutoId: dataAutoId
  }), /*#__PURE__*/_react["default"].createElement("div", {
    className: contentClass
  }, /*#__PURE__*/_react["default"].createElement(_reactRouterDom.Switch, null, data.map(function (tab, index) {
    var key = "tabComponent_".concat(index);
    var route = tab.route,
        exact = tab.exact,
        component = tab.component;
    var TabComponent = component;
    return /*#__PURE__*/_react["default"].createElement(_reactRouterDom.Route, {
      key: key,
      exact: exact,
      path: route,
      component: TabComponent
    });
  }), /*#__PURE__*/_react["default"].createElement(_reactRouterDom.Route, {
    render: function render() {
      return /*#__PURE__*/_react["default"].createElement(_reactRouterDom.Redirect, {
        to: data[defaultSelected].link
      });
    }
  }))));
};

RoutedTabs.propTypes = {
  data: (0, _propTypes.arrayOf)((0, _propTypes.shape)({
    link: _propTypes.string.isRequired,
    route: _propTypes.string.isRequired,
    component: _propTypes.func.isRequired
  })).isRequired,
  fluid: _propTypes.bool,
  className: _propTypes.string,
  contentClassName: _propTypes.string,
  dataAutoId: _propTypes.string,
  defaultSelected: _propTypes.number
};
RoutedTabs.defaultProps = {
  fluid: true,
  className: '',
  contentClassName: '',
  dataAutoId: '',
  defaultSelected: 0
};

var _default = /*#__PURE__*/(0, _react.memo)(RoutedTabs);

exports["default"] = _default;
//# sourceMappingURL=index.js.map