import React, { PureComponent } from 'react'
import { bool, string, func } from 'prop-types'

import { classNames } from '../../util'

import './_style.scss'

const blk = 'dls-toggle-button'

class ToggleButton extends PureComponent {
  changeHandler = ({ target: { checked } }) => {
    const { name, changeHandler } = this.props
    changeHandler({
      [name]: checked,
    })
  }

  render() {
    const {
      dataAutoId,
      id,
      className,
      name,
      disabled,
      active,
      label,
    } = this.props
    const eltClass = classNames({ blk, className })
    const sliderClass = classNames({
      blk,
      elt: 'slider',
      mods: {
        active,
        disabled,
      },
    })
    const labelClass = classNames({ blk, elt: 'label' })
    return (
      <div
        data-auto-id={dataAutoId}
        className={eltClass}
      >
        <div className={classNames({ blk, elt: 'switch' })}>
          <input
            type="checkbox"
            disabled={disabled}
            checked={active}
            className={classNames({ blk, elt: 'check-box' })}
            onChange={this.changeHandler}
            id={id}
            name={name}
          />
          <label
            htmlFor={id}
            className={sliderClass}
          />
        </div>
        <div className={labelClass}>
          {label}
        </div>
      </div>
    )
  }
}

ToggleButton.propTypes = {
  dataAutoId: string,
  id: string.isRequired,
  name: string.isRequired,
  className: string,
  disabled: bool,
  active: bool,
  changeHandler: func,
  label: string,
}

ToggleButton.defaultProps = {
  dataAutoId: 'dlsToggleButton',
  className: '',
  disabled: false,
  active: false,
  changeHandler: () => {},
  label: '',
}

export default ToggleButton
