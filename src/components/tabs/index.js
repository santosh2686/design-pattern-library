import React, { memo, Fragment, useState } from 'react'
import {
  arrayOf, bool, func, node, number, shape, string, oneOfType,
} from 'prop-types'

import { classNames } from '../../util'

import TabList from '../tab-list'

import './_style.scss'

const blk = 'dls-tabs'

const Tabs = ({
  dataAutoId,
  data,
  className,
  contentClassName,
  fluid,
  defaultSelected,
}) => {
  const [selected, selectHandler] = useState(defaultSelected)
  const contentClass = classNames({
    blk,
    elt: 'content',
    className: contentClassName,
  })
  return (
    <>
      <TabList
        dataAutoId={dataAutoId}
        data={data}
        selected={selected}
        selectHandler={selectHandler}
        fluid={fluid}
        className={className}
      />
      <div className={contentClass}>
        {data[selected].component()}
      </div>
    </>
  )
}

Tabs.propTypes = {
  dataAutoId: string,
  data: arrayOf(shape({
    label: oneOfType([node, func]).isRequired,
    component: func.isRequired,
  })).isRequired,
  className: string,
  contentClassName: string,
  fluid: bool,
  defaultSelected: number,
}

Tabs.defaultProps = {
  dataAutoId: 'tabs',
  className: '',
  contentClassName: '',
  fluid: true,
  defaultSelected: 0,
}

export default memo(Tabs)
