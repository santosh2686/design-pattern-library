import React, { memo } from 'react'
import {
  arrayOf, number, string, shape,
} from 'prop-types'

import { classNames } from '../../util'

import Step from './step'

import './_style.scss'

const blk = 'dls-stepper'

const Stepper = ({
  dataAutoId,
  className,
  data,
  activeStep,
}) => {
  const eltClass = classNames({
    blk,
    className,
  })

  return (
    <div data-auto-id={dataAutoId} className={eltClass}>
      {data.map((stepItem, index) => {
        const key = `step-item-${index}`
        return (
          <Step
            key={key}
            step={stepItem}
            activeStep={activeStep}
            stepIndex={index}
          />
        )
      })}
    </div>
  )
}

Stepper.propTypes = {
  dataAutoId: string,
  className: string,
  data: arrayOf(shape({
    label: string,
    className: string,
  })),
  activeStep: number,
}

Stepper.defaultProps = {
  dataAutoId: 'stepper',
  className: '',
  data: [],
  activeStep: 1,
}

export default memo(Stepper)
