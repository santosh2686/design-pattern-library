"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = require("prop-types");

var _util = require("../../util");

var _dropDownSelect = _interopRequireDefault(require("./drop-down-select"));

require("./_style.scss");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var blk = 'dls-drop-down';

var DropDown = /*#__PURE__*/function (_PureComponent) {
  _inherits(DropDown, _PureComponent);

  var _super = _createSuper(DropDown);

  function DropDown() {
    var _this;

    _classCallCheck(this, DropDown);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "state", {
      selectedKey: '',
      selectedValue: ''
    });

    _defineProperty(_assertThisInitialized(_this), "changeHandler", function (e) {
      var changeHandler = _this.props.changeHandler;

      _this.computeSelection(e.target.value, function (selectedOption) {
        changeHandler(selectedOption);
      });
    });

    _defineProperty(_assertThisInitialized(_this), "computeSelection", function (selected) {
      var cb = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : function () {};
      var _this$props = _this.props,
          options = _this$props.options,
          keyMap = _this$props.keyMap,
          valueMap = _this$props.valueMap;
      var selectedOption = options.filter(function (option) {
        return option[keyMap] === selected;
      })[0] || {};

      _this.setState({
        selectedValue: selectedOption[valueMap] || '',
        selectedKey: selectedOption[keyMap] || ''
      }, function () {
        cb(selectedOption);
      });
    });

    return _this;
  }

  _createClass(DropDown, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var selected = this.props.selected;
      this.computeSelection(selected);
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(_ref) {
      var prevSelected = _ref.selected;
      var selected = this.props.selected;

      if (prevSelected !== selected) {
        this.computeSelection(selected);
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          id = _this$props2.id,
          dataAutoId = _this$props2.dataAutoId,
          className = _this$props2.className,
          label = _this$props2.label,
          disabled = _this$props2.disabled,
          placeHolder = _this$props2.placeHolder,
          isRequired = _this$props2.isRequired,
          outlineArrow = _this$props2.outlineArrow,
          state = _this$props2.state,
          options = _this$props2.options,
          keyMap = _this$props2.keyMap,
          valueMap = _this$props2.valueMap;
      var _this$state = this.state,
          selectedKey = _this$state.selectedKey,
          selectedValue = _this$state.selectedValue;
      var eltClass = (0, _util.classNames)({
        blk: blk,
        className: className
      });
      var labelClasses = (0, _util.classNames)({
        blk: blk,
        elt: 'label',
        mods: {
          required: isRequired
        }
      });
      var controlClasses = (0, _util.classNames)({
        blk: blk,
        elt: 'select',
        className: className,
        mods: _defineProperty({}, state, !!state)
      });
      return /*#__PURE__*/_react["default"].createElement("div", {
        "data-auto-id": dataAutoId,
        className: eltClass
      }, label && /*#__PURE__*/_react["default"].createElement("label", {
        htmlFor: id,
        className: labelClasses
      }, label), /*#__PURE__*/_react["default"].createElement("div", {
        className: (0, _util.classNames)({
          blk: blk,
          elt: 'wrapper'
        })
      }, /*#__PURE__*/_react["default"].createElement("select", {
        id: id,
        value: selectedKey,
        className: controlClasses,
        disabled: disabled,
        onChange: this.changeHandler
      }, placeHolder && /*#__PURE__*/_react["default"].createElement("option", {
        value: ""
      }, placeHolder), options.map(function (option) {
        return /*#__PURE__*/_react["default"].createElement("option", {
          key: option[keyMap],
          value: option[keyMap]
        }, option[valueMap]);
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: (0, _util.classNames)({
          blk: blk,
          elt: 'custom'
        })
      }, /*#__PURE__*/_react["default"].createElement(_dropDownSelect["default"], {
        placeHolder: placeHolder,
        selectedValue: selectedValue,
        outlineArrow: outlineArrow
      }))));
    }
  }]);

  return DropDown;
}(_react.PureComponent);

DropDown.propTypes = {
  id: _propTypes.string.isRequired,
  dataAutoId: _propTypes.string,
  className: _propTypes.string,
  label: _propTypes.string,
  disabled: _propTypes.bool,
  outlineArrow: _propTypes.bool,
  selected: _propTypes.string,
  placeHolder: _propTypes.string,
  isRequired: _propTypes.bool,
  state: (0, _propTypes.oneOf)(['valid', 'invalid', '']),
  keyMap: _propTypes.string,
  valueMap: _propTypes.string,
  options: (0, _propTypes.arrayOf)((0, _propTypes.shape)({
    key: _propTypes.string,
    value: _propTypes.string
  })),
  changeHandler: _propTypes.func
};
DropDown.defaultProps = {
  dataAutoId: 'dropDown',
  className: '',
  label: '',
  disabled: false,
  selected: '',
  placeHolder: '',
  isRequired: false,
  outlineArrow: false,
  state: '',
  keyMap: 'key',
  valueMap: 'value',
  options: [],
  changeHandler: function changeHandler() {}
};
var _default = DropDown;
exports["default"] = _default;
//# sourceMappingURL=index.js.map