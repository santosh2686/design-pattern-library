import React, { memo } from 'react'
import {
  string, node, shape, func,
} from 'prop-types'

import { classNames, computeValue } from '../../../util'

import './_style.scss'

const blk = 'dls-body-cell'

const BodyCell = ({ column, dataItem }) => {
  const { custom, map, className } = column
  const eltClass = classNames({
    blk,
    className,
  })

  if (custom) {
    return (
      <td className={eltClass}>
        {custom(dataItem)}
      </td>
    )
  }

  return (
    <td className={eltClass}>
      {computeValue(dataItem, map)}
    </td>
  )
}

BodyCell.propTypes = {
  column: shape({
    title: node,
    map: string,
    className: string,
    custom: func,
  }).isRequired,
  dataItem: shape({}),
}

BodyCell.defaultProps = {
  dataItem: {},
}

export default memo(BodyCell)
