"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = require("prop-types");

var _util = require("../../util");

require("./_style.scss");

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var blk = 'dls-grid-row';

var Row = function Row(_ref) {
  var Tag = _ref.tag,
      children = _ref.children,
      className = _ref.className,
      dataAutoId = _ref.dataAutoId;
  var eltClass = (0, _util.classNames)({
    blk: blk,
    className: className
  });
  return /*#__PURE__*/_react["default"].createElement(Tag, {
    className: eltClass,
    "data-auto-id": dataAutoId
  }, children);
};

Row.propTypes = {
  children: (0, _propTypes.oneOfType)([_propTypes.element, (0, _propTypes.arrayOf)(_propTypes.element)]).isRequired,
  className: _propTypes.string,
  dataAutoId: _propTypes.string,
  tag: _propTypes.string
};
Row.defaultProps = {
  className: '',
  dataAutoId: 'row',
  tag: 'div'
};

var _default = /*#__PURE__*/(0, _react.memo)(Row);

exports["default"] = _default;
//# sourceMappingURL=index.js.map