"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactRouterDom = require("react-router-dom");

var _propTypes = require("prop-types");

var _util = require("../../util");

require("./_style.scss");

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var blk = 'dls-anchor';

var Anchor = function Anchor(_ref) {
  var _mods;

  var category = _ref.category,
      asButton = _ref.asButton,
      asLink = _ref.asLink,
      type = _ref.type,
      href = _ref.href,
      target = _ref.target,
      align = _ref.align,
      disabled = _ref.disabled,
      standalone = _ref.standalone,
      clickHandler = _ref.clickHandler,
      dataAutoId = _ref.dataAutoId,
      className = _ref.className,
      children = _ref.children,
      attributes = _ref.attributes;
  var eltClass = (0, _util.classNames)({
    blk: blk,
    className: className,
    mods: (_mods = {}, _defineProperty(_mods, category, !!category), _defineProperty(_mods, "".concat(category, "-disabled"), disabled), _defineProperty(_mods, "standalone", standalone), _defineProperty(_mods, "align-".concat(align), !!align), _defineProperty(_mods, "button", asButton), _mods)
  });

  if (asButton) {
    return /*#__PURE__*/_react["default"].createElement("button", _extends({
      // eslint-disable-next-line react/button-has-type
      type: type,
      "data-auto-id": dataAutoId,
      className: eltClass,
      onClick: clickHandler,
      disabled: disabled,
      "aria-disabled": disabled // eslint-disable-next-line react/jsx-props-no-spreading

    }, attributes), children);
  }

  if (asLink) {
    return /*#__PURE__*/_react["default"].createElement(_reactRouterDom.NavLink, _extends({
      to: href,
      className: eltClass,
      "data-auto-id": dataAutoId,
      onClick: clickHandler // eslint-disable-next-line react/jsx-props-no-spreading

    }, attributes), children);
  }

  return /*#__PURE__*/_react["default"].createElement("a", _extends({
    "data-auto-id": dataAutoId,
    href: href,
    target: target,
    className: eltClass // eslint-disable-next-line react/jsx-props-no-spreading

  }, attributes), children);
};

Anchor.propTypes = {
  category: (0, _propTypes.oneOf)(['primary', 'secondary']),
  type: (0, _propTypes.oneOf)(['button', 'submit', 'reset']),
  align: (0, _propTypes.oneOf)(['left', 'center', 'right']),
  asButton: _propTypes.bool,
  asLink: _propTypes.bool,
  href: _propTypes.string,
  target: _propTypes.string,
  disabled: _propTypes.bool,
  standalone: _propTypes.bool,
  dataAutoId: _propTypes.string,
  className: _propTypes.string,
  children: _propTypes.node.isRequired,
  clickHandler: _propTypes.func,
  attributes: (0, _propTypes.shape)({})
};
Anchor.defaultProps = {
  category: 'primary',
  align: 'center',
  type: 'button',
  asButton: false,
  asLink: false,
  href: '/',
  target: '_self',
  disabled: false,
  standalone: false,
  dataAutoId: '',
  className: '',
  clickHandler: function clickHandler() {},
  attributes: {}
};

var _default = /*#__PURE__*/(0, _react.memo)(Anchor);

exports["default"] = _default;
//# sourceMappingURL=index.js.map