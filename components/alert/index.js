"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = require("prop-types");

var _util = require("../../util");

var _closeButton = _interopRequireDefault(require("../close-button"));

require("./_style.scss");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var blk = 'dls-alert';

var Alert = function Alert(_ref) {
  var dataAutoId = _ref.dataAutoId,
      className = _ref.className,
      isClosable = _ref.isClosable,
      closeButtonDataAutoId = _ref.closeButtonDataAutoId,
      closeHandler = _ref.closeHandler,
      children = _ref.children;
  var eltClass = (0, _util.classNames)({
    blk: blk,
    className: className
  });
  var contentClass = (0, _util.classNames)({
    blk: blk,
    elt: 'content'
  });
  var closeBtnClass = (0, _util.classNames)({
    blk: blk,
    elt: 'close-button'
  });
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: eltClass,
    "data-auto-id": dataAutoId
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: contentClass
  }, children), isClosable && /*#__PURE__*/_react["default"].createElement(_closeButton["default"], {
    dataAutoId: closeButtonDataAutoId,
    clickHandler: closeHandler,
    className: closeBtnClass
  }));
};

Alert.propTypes = {
  dataAutoId: _propTypes.string,
  className: _propTypes.string,
  isClosable: _propTypes.bool,
  closeButtonDataAutoId: _propTypes.string,
  closeHandler: _propTypes.func,
  children: _propTypes.node.isRequired
};
Alert.defaultProps = {
  dataAutoId: 'alert',
  className: '',
  isClosable: false,
  closeButtonDataAutoId: 'btnClose',
  closeHandler: function closeHandler() {}
};

var _default = /*#__PURE__*/(0, _react.memo)(Alert);

exports["default"] = _default;
//# sourceMappingURL=index.js.map