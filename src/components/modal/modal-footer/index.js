import React, { memo } from 'react'
import { node, string } from 'prop-types'

import { classNames } from '../../../util'

import './_style.scss'

const blk = 'dls-modal-footer'

const ModalFooter = ({
  children,
  className,
}) => {
  const eltClass = classNames({
    blk,
    className,
  })
  return (
    <div className={eltClass}>
      {children}
    </div>
  )
}

ModalFooter.propTypes = {
  children: node,
  className: string,
}

ModalFooter.defaultProps = {
  children: '',
  className: '',
}

export default memo(ModalFooter)
