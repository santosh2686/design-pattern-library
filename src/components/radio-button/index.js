import React, { PureComponent } from 'react'
import { bool, string, func } from 'prop-types'

import { classNames } from '../../util'

import './_style.scss'

const blk = 'dls-radio-button'

class RadioButton extends PureComponent {
  handleChange = () => {
    const { name, value, changeHandler } = this.props
    changeHandler({ [name]: value })
  };

  render() {
    const {
      dataAutoId, id, name, value,
      label, className, disabled, checked,
    } = this.props
    const eltClass = classNames({
      blk,
      className,
    })
    const controlClass = classNames({
      blk,
      elt: 'input',
    })
    const labelClass = classNames({
      blk,
      elt: 'label',
    })
    return (
      <div
        data-auto-id={dataAutoId}
        className={eltClass}
      >
        <input
          type="radio"
          id={id}
          name={name}
          value={value}
          className={controlClass}
          disabled={disabled}
          checked={checked}
          onClick={this.handleChange}
          readOnly
        />
        <label
          htmlFor={id}
          className={labelClass}
        >
          {label}
        </label>
      </div>
    )
  }
}

RadioButton.propTypes = {
  label: string,
  dataAutoId: string,
  id: string.isRequired,
  name: string.isRequired,
  value: string.isRequired,
  className: string,
  disabled: bool,
  checked: bool,
  changeHandler: func,
}

RadioButton.defaultProps = {
  label: '',
  dataAutoId: 'radioButton',
  className: '',
  disabled: false,
  checked: false,
  changeHandler: () => {},
}

export default RadioButton
