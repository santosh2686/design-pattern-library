import React, { memo } from 'react'
import {
  oneOfType, string, element, arrayOf,
} from 'prop-types'

import { classNames } from '../../util'

import './_style.scss'

const blk = 'dls-grid-row'

const Row = ({
  tag: Tag,
  children,
  className,
  dataAutoId,
}) => {
  const eltClass = classNames({
    blk,
    className,
  })

  return (
    <Tag
      className={eltClass}
      data-auto-id={dataAutoId}
    >
      {children}
    </Tag>
  )
}

Row.propTypes = {
  children: oneOfType([
    element,
    arrayOf(element),
  ]).isRequired,
  className: string,
  dataAutoId: string,
  tag: string,
}

Row.defaultProps = {
  className: '',
  dataAutoId: 'row',
  tag: 'div',
}

export default memo(Row)
