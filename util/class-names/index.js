"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _cssBemClasses = _interopRequireDefault(require("css-bem-classes"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var classNames = function classNames(_ref) {
  var _ref$blk = _ref.blk,
      blk = _ref$blk === void 0 ? '' : _ref$blk,
      _ref$elt = _ref.elt,
      elt = _ref$elt === void 0 ? '' : _ref$elt,
      _ref$mods = _ref.mods,
      mods = _ref$mods === void 0 ? {} : _ref$mods,
      _ref$className = _ref.className,
      className = _ref$className === void 0 ? '' : _ref$className;
  var cn = (0, _cssBemClasses["default"])(blk, {
    element: '__',
    modifier: '--'
  });
  var classString = cn(mods).trim();

  if (elt) {
    classString = cn(elt, mods).trim();
  }

  classString = className ? "".concat(classString, " ").concat(className) : classString;
  return classString.trim();
};

var _default = classNames;
exports["default"] = _default;
//# sourceMappingURL=index.js.map