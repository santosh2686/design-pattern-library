import React, { PureComponent } from 'react'
import { node, string } from 'prop-types'

import { classNames } from '../../../util'

import './_style.scss'

const blk = 'dls-tooltip-wrapper'

class TooltipWrapper extends PureComponent {
  render() {
    const {
      className, children, category, position, align,
    } = this.props

    const eltClass = classNames({
      blk,
      className,
      mods: {
        [position]: !!position,
        [align]: !!align,
      },
    })

    const arrowClass = classNames({
      blk,
      elt: 'arrow',
      mods: {
        [`${category}-${position}`]: !!position && !!category,
        [align]: !!align,
      },
    })

    return (
      <div className={eltClass}>
        {children}
        <div className={arrowClass} />
      </div>
    )
  }
}

TooltipWrapper.propTypes = {
  children: node.isRequired,
  className: string,
  position: string.isRequired,
  align: string.isRequired,
  category: string.isRequired,
}

TooltipWrapper.defaultProps = {
  className: '',
}

export default TooltipWrapper
