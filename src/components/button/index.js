/* eslint-disable react/button-has-type */
import React, { memo } from 'react'
import { NavLink } from 'react-router-dom'
import {
  bool, func, oneOf, node, string, shape,
} from 'prop-types'

import { classNames } from '../../util'

import Spinner from '../spinner'

import './_style.scss'

const blk = 'dls-button'

const Button = ({
  dataAutoId,
  className,
  type,
  category,
  size,
  children,
  href,
  target,
  asAnchor,
  asLink,
  fluid,
  loading,
  loadingWithLabel,
  disabled,
  clickHandler,
  ariaProps,
}) => {
  const eltClass = classNames({
    blk,
    className,
    mods: {
      [category]: !!category,
      [size]: !!size,
      fluid,
      disabled,
      loading,
    },
  })

  const loaderWrapper = classNames({
    blk,
    elt: 'loader-wrapper',
    mods: {
      [category]: !!category,
      'with-no-label': !loadingWithLabel,
      'with-label': loadingWithLabel,
    },
  })

  const loaderClasses = classNames({
    blk,
    elt: 'loader',
    mods: {
      [category]: !!category,
      [size]: !!size,
    },
  })

  if (asAnchor) {
    return (
      <a
        data-auto-id={dataAutoId}
        className={eltClass}
        href={href}
        target={target}
        onClick={clickHandler}
      >
        {children}
      </a>
    )
  }

  if (asLink) {
    return (
      <NavLink
        data-auto-id={dataAutoId}
        className={eltClass}
        to={href}
        onClick={clickHandler}
      >
        {children}
      </NavLink>
    )
  }

  return (
    <button
      data-auto-id={dataAutoId}
      className={eltClass}
      type={type}
      disabled={disabled}
      onClick={clickHandler}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...ariaProps}
    >
      {loading && (
        <div className={loaderWrapper}>
          <Spinner className={loaderClasses} />
        </div>
      )}
      {children}
    </button>
  )
}

Button.propTypes = {
  dataAutoId: string,
  type: oneOf(['button', 'submit', 'reset']),
  category: string,
  size: string,
  children: node.isRequired,
  className: string,
  href: string,
  target: string,
  loading: bool,
  loadingWithLabel: bool,
  asAnchor: bool,
  asLink: bool,
  fluid: bool,
  disabled: bool,
  clickHandler: func,
  ariaProps: shape({
    'aria-label': string,
  }),
}

Button.defaultProps = {
  dataAutoId: 'button',
  type: 'button',
  category: 'primary',
  size: 'default',
  className: '',
  href: '/',
  target: '_self',
  loading: false,
  loadingWithLabel: false,
  asAnchor: false,
  asLink: false,
  fluid: false,
  disabled: false,
  clickHandler: () => {},
  ariaProps: {},
}

export default memo(Button)
