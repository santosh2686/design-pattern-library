import React, { memo, Fragment, useState } from 'react'
import { NavLink } from 'react-router-dom'
import {
  bool, node, string, shape,
} from 'prop-types'

import { classNames } from '../../../util'

import Icon from '../../icon'

import plusIcon from '../icon/plus.svg'
import minusIcon from '../icon/minus.svg'
import disableIcon from '../icon/disable.svg'

import './_style.scss'

const blk = 'dls-menu-item'

const MenuItem = ({
  item,
  withChildren,
  children,
}) => {
  const { label, route = '/', expanded = false } = item
  const [isExpanded, toggleExpand] = useState(expanded)
  const subMenuClass = classNames({
    blk,
    elt: 'sub-menu',
    mods: {
      expanded: isExpanded,
    },
  })
  if (withChildren) {
    return (
      <>
        <button
          type="button"
          className={classNames({ blk, elt: 'button' })}
          onClick={() => toggleExpand(!isExpanded)}
        >
          <Icon
            className={classNames({ blk, elt: 'icon' })}
            icon={isExpanded ? minusIcon : plusIcon}
            isCustomSize
            isCustomColor
          />
          <span className={classNames({ blk, elt: 'label' })}>
            {label}
          </span>
        </button>
        <div className={subMenuClass}>
          {children}
        </div>
      </>
    )
  }

  return (
    <NavLink
      to={route}
      exact
      className={classNames({ blk, elt: 'link' })}
    >
      <Icon
        icon={disableIcon}
        className={classNames({
          blk,
          elt: 'icon',
          mods: { 'link-icon': true },
        })}
        isCustomSize
        isCustomColor
      />
      <span className={classNames({ blk, elt: 'label' })}>
        {label}
      </span>
    </NavLink>
  )
}

MenuItem.propTypes = {
  item: shape({
    label: string,
    route: string,
  }).isRequired,
  withChildren: bool,
  children: node,
}

MenuItem.defaultProps = {
  children: null,
  withChildren: false,
}

export default memo(MenuItem)
