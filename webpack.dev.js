const path = require('path');

module.exports = {
  entry: './index.js',
  mode: 'development',
  devtool: 'inline-source-map',
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'assets/bundle.[name].js',
    publicPath: '/',
  },
  devServer: {
    port: process.env.PORT || 4040,
    host: '0.0.0.0',
    historyApiFallback: true,
    disableHostCheck: true,
    overlay: true,
    compress: true,
  },
  resolve: {
    extensions: ['.jsx', '.js', '.json'],
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
    ],
  },
  plugins: [],
}
