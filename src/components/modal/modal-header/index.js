import React, { memo } from 'react'
import { func, node, string } from 'prop-types'

import { classNames } from '../../../util'

import CloseButton from '../../close-button'

import './_style.scss'

const blk = 'dls-modal-header'

const ModalHeader = ({
  children,
  closeHandler,
  className,
}) => {
  const eltClass = classNames({
    blk,
    className,
  })
  const contentClass = classNames({
    blk,
    elt: 'content',
  })
  const closeBtnClass = classNames({
    blk,
    elt: 'close-button',
  })
  return (
    <div className={eltClass}>
      <div className={contentClass}>
        {children}
      </div>
      <CloseButton
        dataAutoId="btnModalClose"
        clickHandler={closeHandler}
        className={closeBtnClass}
      />
    </div>
  )
}

ModalHeader.propTypes = {
  children: node,
  closeHandler: func,
  className: string,
}

ModalHeader.defaultProps = {
  children: '',
  closeHandler: () => {},
  className: '',
}

export default memo(ModalHeader)
