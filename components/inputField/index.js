"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = require("prop-types");

var _util = require("../../util");

require("./_style.scss");

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var blk = 'dls-input-field';

var InputField = /*#__PURE__*/function (_PureComponent) {
  _inherits(InputField, _PureComponent);

  var _super = _createSuper(InputField);

  function InputField() {
    var _this;

    _classCallCheck(this, InputField);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "changeHandler", function (event) {
      var value = event.target.value;
      var _this$props = _this.props,
          name = _this$props.name,
          changeHandler = _this$props.changeHandler;
      changeHandler(_defineProperty({}, name, value));
    });

    return _this;
  }

  _createClass(InputField, [{
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          dataAutoId = _this$props2.dataAutoId,
          id = _this$props2.id,
          label = _this$props2.label,
          value = _this$props2.value,
          name = _this$props2.name,
          type = _this$props2.type,
          placeholder = _this$props2.placeholder,
          isRequired = _this$props2.isRequired,
          state = _this$props2.state,
          maxLength = _this$props2.maxLength,
          disabled = _this$props2.disabled,
          attributes = _this$props2.attributes,
          className = _this$props2.className;
      var labelClasses = (0, _util.classNames)({
        blk: blk,
        elt: 'label',
        mods: {
          required: isRequired
        }
      });
      var controlClasses = (0, _util.classNames)({
        blk: blk,
        elt: 'input',
        className: className,
        mods: _defineProperty({}, state, !!state)
      });
      return /*#__PURE__*/_react["default"].createElement("div", {
        className: blk
      }, /*#__PURE__*/_react["default"].createElement("label", {
        className: labelClasses
      }, label), /*#__PURE__*/_react["default"].createElement("input", _extends({
        id: id,
        type: type,
        name: name,
        value: value,
        className: controlClasses,
        disabled: disabled,
        maxLength: maxLength,
        "data-auto-id": dataAutoId,
        placeholder: placeholder,
        onChange: this.changeHandler // eslint-disable-next-line react/jsx-props-no-spreading

      }, attributes)));
    }
  }]);

  return InputField;
}(_react.PureComponent);

InputField.propTypes = {
  dataAutoId: _propTypes.string,
  id: _propTypes.string,
  label: _propTypes.string,
  value: _propTypes.string,
  name: _propTypes.string,
  placeholder: _propTypes.string,
  isRequired: _propTypes.bool,
  state: (0, _propTypes.oneOf)(['valid', 'invalid', '']),
  type: _propTypes.string,
  message: _propTypes.string,
  icon: _propTypes.string,
  labelInfo: _propTypes.string,
  showCharCount: _propTypes.bool,
  maxLength: _propTypes.number,
  changeHandler: _propTypes.func.isRequired,
  disabled: _propTypes.bool,
  className: _propTypes.string,
  controlClassName: _propTypes.string,
  isTextArea: _propTypes.bool,
  attributes: (0, _propTypes.shape)({})
};
InputField.defaultProps = {
  dataAutoId: '',
  id: '',
  label: '',
  value: '',
  name: '',
  type: 'text',
  placeholder: '',
  isRequired: false,
  state: '',
  message: '',
  icon: '',
  labelInfo: '',
  showCharCount: false,
  maxLength: 80,
  disabled: false,
  className: '',
  controlClassName: '',
  isTextArea: false,
  attributes: {}
};
var _default = InputField;
exports["default"] = _default;
//# sourceMappingURL=index.js.map