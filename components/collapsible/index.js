"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = require("prop-types");

var _util = require("../../util");

require("./_style.scss");

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var blk = 'dls-collapsible';

var Collapsible = function Collapsible(_ref) {
  var dataAutoId = _ref.dataAutoId,
      className = _ref.className,
      headerClassName = _ref.headerClassName,
      containerClassName = _ref.containerClassName,
      title = _ref.title,
      actionElement = _ref.actionElement,
      children = _ref.children,
      isExpanded = _ref.isExpanded;
  var eltClass = (0, _util.classNames)({
    blk: blk,
    className: className
  });
  var headerClass = (0, _util.classNames)({
    blk: blk,
    elt: 'header',
    className: headerClassName
  });
  var containerClass = (0, _util.classNames)({
    blk: blk,
    elt: 'container',
    className: containerClassName
  });
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: eltClass,
    "data-auto-id": dataAutoId
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: headerClass
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: (0, _util.classNames)({
      blk: blk,
      elt: 'title'
    })
  }, title), /*#__PURE__*/_react["default"].createElement("div", null, actionElement)), isExpanded && /*#__PURE__*/_react["default"].createElement("div", {
    className: containerClass
  }, children));
};

Collapsible.propTypes = {
  dataAutoId: _propTypes.string,
  className: _propTypes.string,
  containerClassName: _propTypes.string,
  headerClassName: _propTypes.string,
  title: (0, _propTypes.oneOfType)([_propTypes.string, _propTypes.node]),
  actionElement: _propTypes.node,
  children: _propTypes.node.isRequired,
  isExpanded: _propTypes.bool
};
Collapsible.defaultProps = {
  dataAutoId: 'collapsible',
  className: '',
  containerClassName: '',
  headerClassName: '',
  title: '',
  actionElement: null,
  isExpanded: true
};

var _default = /*#__PURE__*/(0, _react.memo)(Collapsible);

exports["default"] = _default;
//# sourceMappingURL=index.js.map