import React, { memo } from 'react'
import { string } from 'prop-types'

import MenuItem from '../menu-item'

const ExpandableMenuItem = ({ data }) => data.map((menu) => {
  if (menu.items) {
    return (
      <MenuItem item={menu} withChildren>
        <ExpandableMenuItem
          data={menu.items}
          className={menu.className}
        />
      </MenuItem>
    )
  }

  return (
    <MenuItem item={menu} />
  )
})

ExpandableMenuItem.propTypes = {
  className: string,
}

ExpandableMenuItem.defaultProps = {
  className: '',
}

export default memo(ExpandableMenuItem)
