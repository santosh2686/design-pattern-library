import React, { PureComponent } from 'react'
import {
  bool, string, oneOf, shape, number, func,
} from 'prop-types'

import { classNames } from '../../util'

import './_style.scss'

const blk = 'dls-drop-down'

class DropDown extends PureComponent {
  render() {
    return (
      <div>
        Dropdown
      </div>
    )
  }
}

export default DropDown
