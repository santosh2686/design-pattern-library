import React, { memo } from 'react'
import {
  bool, string, func, node,
} from 'prop-types'

import { classNames } from '../../util'

import CloseButton from '../close-button'

import './_style.scss'

const blk = 'dls-alert'

const Alert = ({
  dataAutoId,
  className,
  isClosable,
  closeButtonDataAutoId,
  closeHandler,
  children,
}) => {
  const eltClass = classNames({
    blk,
    className,
  })

  const contentClass = classNames({
    blk,
    elt: 'content',
  })

  const closeBtnClass = classNames({
    blk,
    elt: 'close-button',
  })

  return (
    <div className={eltClass} data-auto-id={dataAutoId}>
      <div className={contentClass}>
        {children}
      </div>
      {isClosable && (
        <CloseButton
          dataAutoId={closeButtonDataAutoId}
          clickHandler={closeHandler}
          className={closeBtnClass}
        />
      )}
    </div>
  )
}

Alert.propTypes = {
  dataAutoId: string,
  className: string,
  isClosable: bool,
  closeButtonDataAutoId: string,
  closeHandler: func,
  children: node.isRequired,
}

Alert.defaultProps = {
  dataAutoId: 'alert',
  className: '',
  isClosable: false,
  closeButtonDataAutoId: 'btnClose',
  closeHandler: () => {},
}

export default memo(Alert)
