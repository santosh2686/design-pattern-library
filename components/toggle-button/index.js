"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = require("prop-types");

var _util = require("../../util");

require("./_style.scss");

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var blk = 'dls-toggle-button';

var ToggleButton = /*#__PURE__*/function (_PureComponent) {
  _inherits(ToggleButton, _PureComponent);

  var _super = _createSuper(ToggleButton);

  function ToggleButton() {
    var _this;

    _classCallCheck(this, ToggleButton);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "changeHandler", function (_ref) {
      var checked = _ref.target.checked;
      var _this$props = _this.props,
          name = _this$props.name,
          changeHandler = _this$props.changeHandler;
      changeHandler(_defineProperty({}, name, checked));
    });

    return _this;
  }

  _createClass(ToggleButton, [{
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          dataAutoId = _this$props2.dataAutoId,
          id = _this$props2.id,
          className = _this$props2.className,
          name = _this$props2.name,
          disabled = _this$props2.disabled,
          active = _this$props2.active,
          label = _this$props2.label;
      var eltClass = (0, _util.classNames)({
        blk: blk,
        className: className
      });
      var sliderClass = (0, _util.classNames)({
        blk: blk,
        elt: 'slider',
        mods: {
          active: active,
          disabled: disabled
        }
      });
      var labelClass = (0, _util.classNames)({
        blk: blk,
        elt: 'label'
      });
      return /*#__PURE__*/_react["default"].createElement("div", {
        "data-auto-id": dataAutoId,
        className: eltClass
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: (0, _util.classNames)({
          blk: blk,
          elt: 'switch'
        })
      }, /*#__PURE__*/_react["default"].createElement("input", {
        type: "checkbox",
        disabled: disabled,
        checked: active,
        className: (0, _util.classNames)({
          blk: blk,
          elt: 'check-box'
        }),
        onChange: this.changeHandler,
        id: id,
        name: name
      }), /*#__PURE__*/_react["default"].createElement("label", {
        htmlFor: id,
        className: sliderClass
      })), /*#__PURE__*/_react["default"].createElement("div", {
        className: labelClass
      }, label));
    }
  }]);

  return ToggleButton;
}(_react.PureComponent);

ToggleButton.propTypes = {
  dataAutoId: _propTypes.string,
  id: _propTypes.string.isRequired,
  name: _propTypes.string.isRequired,
  className: _propTypes.string,
  disabled: _propTypes.bool,
  active: _propTypes.bool,
  changeHandler: _propTypes.func,
  label: _propTypes.string
};
ToggleButton.defaultProps = {
  dataAutoId: 'dlsToggleButton',
  className: '',
  disabled: false,
  active: false,
  changeHandler: function changeHandler() {},
  label: ''
};
var _default = ToggleButton;
exports["default"] = _default;
//# sourceMappingURL=index.js.map