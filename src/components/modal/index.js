import React, { memo } from 'react'
import { node, string } from 'prop-types'

import { classNames } from '../../util'

import ModalHeader from './modal-header'
import ModalBody from './modal-body'
import ModalFooter from './modal-footer'

import './_style.scss'

const blk = 'dls-modal'

const Modal = ({
  children,
  className,
}) => {
  const containerClass = classNames({
    blk,
    elt: 'container',
    className,
  })

  return (
    <div className={blk}>
      <div className={containerClass}>
        {children}
      </div>
    </div>
  )
}

Modal.propTypes = {
  children: node,
  className: string,
}

Modal.defaultProps = {
  children: '',
  className: '',
}

const ModalWithMemo = memo(Modal)

export {
  ModalWithMemo as default,
  ModalHeader,
  ModalBody,
  ModalFooter,
}
