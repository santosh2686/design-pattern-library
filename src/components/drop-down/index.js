import React, { PureComponent } from 'react'
import {
  arrayOf, bool, string, oneOf, shape, func,
} from 'prop-types'

import { classNames } from '../../util'

import DropDownSelect from './drop-down-select'

import './_style.scss'

const blk = 'dls-drop-down'

class DropDown extends PureComponent {
  state = {
    selectedKey: '',
    selectedValue: '',
  }

  componentDidMount() {
    const { selected } = this.props
    this.computeSelection(selected)
  }

  componentDidUpdate({ selected: prevSelected }) {
    const { selected } = this.props
    if (prevSelected !== selected) {
      this.computeSelection(selected)
    }
  }

  changeHandler = (e) => {
    const { changeHandler } = this.props
    this.computeSelection(e.target.value, (selectedOption) => {
      changeHandler(selectedOption)
    })
  }

  computeSelection = (selected, cb = () => {}) => {
    const {
      options, keyMap, valueMap,
    } = this.props
    const selectedOption = options.filter((option) => option[keyMap] === selected)[0] || {}
    this.setState({
      selectedValue: selectedOption[valueMap] || '',
      selectedKey: selectedOption[keyMap] || '',
    }, () => {
      cb(selectedOption)
    })
  }

  render() {
    const {
      id,
      dataAutoId,
      className,
      label,
      disabled,
      placeHolder,
      isRequired,
      outlineArrow,
      state,
      options,
      keyMap,
      valueMap,
    } = this.props
    const { selectedKey, selectedValue } = this.state
    const eltClass = classNames({ blk, className })
    const labelClasses = classNames({
      blk,
      elt: 'label',
      mods: {
        required: isRequired,
      },
    })
    const controlClasses = classNames({
      blk,
      elt: 'select',
      className,
      mods: {
        [state]: !!state,
      },
    })
    return (
      <div data-auto-id={dataAutoId} className={eltClass}>
        {label
        && (
          <label htmlFor={id} className={labelClasses}>
            {label}
          </label>
        )}
        <div className={classNames({ blk, elt: 'wrapper' })}>
          <select
            id={id}
            value={selectedKey}
            className={controlClasses}
            disabled={disabled}
            onChange={this.changeHandler}
          >
            {placeHolder && <option value="">{placeHolder}</option>}
            {options.map((option) => (
              <option key={option[keyMap]} value={option[keyMap]}>
                {option[valueMap]}
              </option>
            ))}
          </select>
          <div className={classNames({ blk, elt: 'custom' })}>
            <DropDownSelect
              placeHolder={placeHolder}
              selectedValue={selectedValue}
              outlineArrow={outlineArrow}
            />
          </div>
        </div>
      </div>
    )
  }
}

DropDown.propTypes = {
  id: string.isRequired,
  dataAutoId: string,
  className: string,
  label: string,
  disabled: bool,
  outlineArrow: bool,
  selected: string,
  placeHolder: string,
  isRequired: bool,
  state: oneOf(['valid', 'invalid', '']),
  keyMap: string,
  valueMap: string,
  options: arrayOf(shape({
    key: string,
    value: string,
  })),
  changeHandler: func,
}

DropDown.defaultProps = {
  dataAutoId: 'dropDown',
  className: '',
  label: '',
  disabled: false,
  selected: '',
  placeHolder: '',
  isRequired: false,
  outlineArrow: false,
  state: '',
  keyMap: 'key',
  valueMap: 'value',
  options: [],
  changeHandler: () => {},
}

export default DropDown
