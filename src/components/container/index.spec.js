import React from 'react'
import { shallow } from 'enzyme'

import Container from './index'

describe('Container Component', () => {
  let wrapper
  beforeAll(() => {
    const component = (
      <Container>
        Container content
      </Container>
    )
    wrapper = shallow(component)
  })

  it('should render', () => {
    expect(wrapper).toBeDefined()
  })
})
