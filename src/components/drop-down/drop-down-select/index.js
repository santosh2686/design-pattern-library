import React, { memo, Fragment } from 'react'
import { bool, string } from 'prop-types'

import { classNames } from '../../../util'

import './_style.scss'

const blk = 'asda-drop-down-select'

const DropDownSelect = ({
  placeHolder,
  selectedValue,
  outlineArrow,
}) => {
  const valueClasses = classNames({
    blk,
    elt: 'value',
    mods: {
      placeHolder: !!placeHolder && !selectedValue,
    },
  })
  const arrowClasses = classNames({
    blk,
    elt: 'arrow',
    mods: {
      outline: outlineArrow,
    },
  })
  return (
    <>
      <div className={valueClasses}>
        {selectedValue || placeHolder}
      </div>
      <span className={arrowClasses} />
    </>
  )
}

DropDownSelect.propTypes = {
  placeHolder: string,
  selectedValue: string,
  outlineArrow: bool,
}

DropDownSelect.defaultProps = {
  placeHolder: 'Select a value',
  selectedValue: '',
  outlineArrow: false,
}

export default memo(DropDownSelect)
