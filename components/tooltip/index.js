"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = require("prop-types");

var _util = require("../../util");

var _tooltipWrapper = _interopRequireDefault(require("./tooltip-wrapper"));

var _tooltipContent = _interopRequireDefault(require("./tooltip-content"));

require("./_style.scss");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var blk = 'dls-tooltip';

var ToolTip = function ToolTip(_ref) {
  var dataAutoId = _ref.dataAutoId,
      className = _ref.className,
      wrapperClass = _ref.wrapperClass,
      contentClass = _ref.contentClass,
      category = _ref.category,
      position = _ref.position,
      align = _ref.align,
      children = _ref.children,
      content = _ref.content,
      isClosable = _ref.isClosable,
      closeButtonDataAutoId = _ref.closeButtonDataAutoId,
      closeHandler = _ref.closeHandler;
  var eltClass = (0, _util.classNames)({
    blk: blk,
    wrapperClass: wrapperClass
  });
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: eltClass,
    "data-auto-id": dataAutoId
  }, children, /*#__PURE__*/_react["default"].createElement(_tooltipWrapper["default"], {
    className: className,
    position: position,
    align: align,
    category: category
  }, /*#__PURE__*/_react["default"].createElement(_tooltipContent["default"], {
    className: contentClass,
    isClosable: isClosable,
    dataAutoId: closeButtonDataAutoId,
    closeHandler: closeHandler,
    category: category
  }, content)));
};

ToolTip.propTypes = {
  dataAutoId: _propTypes.string,
  className: _propTypes.string,
  wrapperClass: _propTypes.string,
  contentClass: _propTypes.string,
  category: _propTypes.string,
  position: (0, _propTypes.oneOf)(['top', 'bottom']),
  align: (0, _propTypes.oneOf)(['left', 'center', 'right']),
  children: _propTypes.node.isRequired,
  content: _propTypes.node.isRequired,
  closeHandler: _propTypes.func,
  closeButtonDataAutoId: _propTypes.string,
  isClosable: _propTypes.bool
};
ToolTip.defaultProps = {
  dataAutoId: 'dlsTooltip',
  className: '',
  wrapperClass: '',
  contentClass: '',
  category: 'primary',
  position: 'top',
  align: 'center',
  closeHandler: function closeHandler() {},
  closeButtonDataAutoId: 'btnClose',
  isClosable: false
};

var _default = /*#__PURE__*/(0, _react.memo)(ToolTip);

exports["default"] = _default;
//# sourceMappingURL=index.js.map