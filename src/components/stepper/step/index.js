import React, { memo } from 'react'
import { arrayOf, number, shape, string } from 'prop-types'
import { classNames } from '../../../util'

import './_style.scss'

const blk = 'dls-stepper-step'

const Step = ({
  step,
  activeStep,
  stepIndex,
}) => {
  const { label, className } = step
  const currentStep = activeStep - 1
  const mods = {
    active: currentStep === stepIndex,
    completed: stepIndex < currentStep,
  }
  const eltClass = classNames({
    blk,
    className,
    mods,
  })
  const stepNumberClass = classNames({
    blk,
    elt: 'number',
    mods,
  })
  const stepLabelClass = classNames({
    blk,
    elt: 'label',
    mods,
  })

  return (
    <div className={eltClass}>
      <span className={stepNumberClass}>
        {stepIndex + 1}
      </span>
      <span className={stepLabelClass}>
        {label}
      </span>
    </div>
  )
}

Step.propTypes = {
  step: arrayOf(shape({
    label: string,
    className: string,
  })).isRequired,
  activeStep: number.isRequired,
  stepIndex: number.isRequired,
}

export default memo(Step)
