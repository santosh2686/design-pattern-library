"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactRouterDom = require("react-router-dom");

var _propTypes = require("prop-types");

var _util = require("../../util");

var _spinner = _interopRequireDefault(require("../spinner"));

require("./_style.scss");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var blk = 'dls-button';

var Button = function Button(_ref) {
  var _mods, _mods2, _mods3;

  var dataAutoId = _ref.dataAutoId,
      className = _ref.className,
      type = _ref.type,
      category = _ref.category,
      size = _ref.size,
      children = _ref.children,
      href = _ref.href,
      target = _ref.target,
      asAnchor = _ref.asAnchor,
      asLink = _ref.asLink,
      fluid = _ref.fluid,
      loading = _ref.loading,
      loadingWithLabel = _ref.loadingWithLabel,
      disabled = _ref.disabled,
      clickHandler = _ref.clickHandler,
      ariaProps = _ref.ariaProps;
  var eltClass = (0, _util.classNames)({
    blk: blk,
    className: className,
    mods: (_mods = {}, _defineProperty(_mods, category, !!category), _defineProperty(_mods, size, !!size), _defineProperty(_mods, "fluid", fluid), _defineProperty(_mods, "disabled", disabled), _defineProperty(_mods, "loading", loading), _mods)
  });
  var loaderWrapper = (0, _util.classNames)({
    blk: blk,
    elt: 'loader-wrapper',
    mods: (_mods2 = {}, _defineProperty(_mods2, category, !!category), _defineProperty(_mods2, 'with-no-label', !loadingWithLabel), _defineProperty(_mods2, 'with-label', loadingWithLabel), _mods2)
  });
  var loaderClasses = (0, _util.classNames)({
    blk: blk,
    elt: 'loader',
    mods: (_mods3 = {}, _defineProperty(_mods3, category, !!category), _defineProperty(_mods3, size, !!size), _mods3)
  });

  if (asAnchor) {
    return /*#__PURE__*/_react["default"].createElement("a", {
      "data-auto-id": dataAutoId,
      className: eltClass,
      href: href,
      target: target,
      onClick: clickHandler
    }, children);
  }

  if (asLink) {
    return /*#__PURE__*/_react["default"].createElement(_reactRouterDom.NavLink, {
      "data-auto-id": dataAutoId,
      className: eltClass,
      to: href,
      onClick: clickHandler
    }, children);
  }

  return /*#__PURE__*/_react["default"].createElement("button", _extends({
    "data-auto-id": dataAutoId,
    className: eltClass,
    type: type,
    disabled: disabled,
    onClick: clickHandler // eslint-disable-next-line react/jsx-props-no-spreading

  }, ariaProps), loading && /*#__PURE__*/_react["default"].createElement("div", {
    className: loaderWrapper
  }, /*#__PURE__*/_react["default"].createElement(_spinner["default"], {
    className: loaderClasses
  })), children);
};

Button.propTypes = {
  dataAutoId: _propTypes.string,
  type: (0, _propTypes.oneOf)(['button', 'submit', 'reset']),
  category: _propTypes.string,
  size: _propTypes.string,
  children: _propTypes.node.isRequired,
  className: _propTypes.string,
  href: _propTypes.string,
  target: _propTypes.string,
  loading: _propTypes.bool,
  loadingWithLabel: _propTypes.bool,
  asAnchor: _propTypes.bool,
  asLink: _propTypes.bool,
  fluid: _propTypes.bool,
  disabled: _propTypes.bool,
  clickHandler: _propTypes.func,
  ariaProps: (0, _propTypes.shape)({
    'aria-label': _propTypes.string
  })
};
Button.defaultProps = {
  dataAutoId: 'button',
  type: 'button',
  category: 'primary',
  size: 'default',
  className: '',
  href: '/',
  target: '_self',
  loading: false,
  loadingWithLabel: false,
  asAnchor: false,
  asLink: false,
  fluid: false,
  disabled: false,
  clickHandler: function clickHandler() {},
  ariaProps: {}
};

var _default = /*#__PURE__*/(0, _react.memo)(Button);

exports["default"] = _default;
//# sourceMappingURL=index.js.map