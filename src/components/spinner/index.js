import React, { memo } from 'react'
import { bool, string } from 'prop-types'

import { classNames } from '../../util'

import './_style.scss'

const blk = 'dls-spinner'

const Spinner = ({
  dataAutoId,
  className,
  category,
  size,
  isCenter,
}) => {
  const eltClass = classNames({
    blk,
    mods: {
      [category]: !!category,
      [size]: !!size,
      center: isCenter,
    },
    className,
  })
  return (
    <span className={eltClass} data-auto-id={dataAutoId} />
  )
}

Spinner.propTypes = {
  dataAutoId: string,
  className: string,
  category: string,
  size: string,
  isCenter: bool,
}

Spinner.defaultProps = {
  dataAutoId: 'spinner',
  className: '',
  category: 'primary',
  size: 'default',
  isCenter: true,
}

export default memo(Spinner)
