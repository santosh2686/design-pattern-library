import React, { memo } from 'react'
import { NavLink } from 'react-router-dom'
import {
  bool, func, node, oneOfType, string,
} from 'prop-types'

import { classNames } from '../../util'

import './_style.scss'

const blk = 'dls-tab-button'

const TabButton = ({
  className,
  label,
  link,
  fluid,
  selected,
  isRouted,
  selectHandler,
}) => {
  const linkEltClass = classNames({
    blk,
    className,
    mods: {
      fluid,
    },
  })

  const tabLabel = label instanceof Function ? label() : label

  if (isRouted) {
    return (
      <NavLink
        to={link}
        exact
        className={linkEltClass}
        activeClassName="dls-tab-button--selected"
      >
        {tabLabel}
      </NavLink>
    )
  }

  const buttonEltClass = classNames({
    blk,
    className,
    mods: {
      fluid,
      selected,
    },
  })

  return (
    <button
      type="button"
      onClick={selectHandler}
      className={buttonEltClass}
      role="tab"
      aria-selected={selected}
    >
      {tabLabel}
    </button>
  )
}

TabButton.propTypes = {
  className: string,
  label: oneOfType([node, func]).isRequired,
  link: string,
  fluid: bool,
  selected: bool,
  isRouted: bool,
  selectHandler: func,
}

TabButton.defaultProps = {
  className: '',
  link: '/',
  fluid: true,
  selected: false,
  isRouted: false,
  selectHandler: () => {},
}

export default memo(TabButton)
