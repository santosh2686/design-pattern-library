"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = require("prop-types");

var _util = require("../../util");

require("./_style.scss");

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var blk = 'dls-grid-column';

var Column = function Column(_ref) {
  var _mods;

  var Tag = _ref.tag,
      children = _ref.children,
      col = _ref.col,
      offset = _ref.offset,
      className = _ref.className,
      dataAutoId = _ref.dataAutoId;
  var xxlg = col.xxlg,
      xlg = col.xlg,
      lg = col.lg,
      md = col.md,
      sm = col.sm,
      xs = col.xs;
  var offsetXxlg = offset.xxlg,
      offsetXlg = offset.xlg,
      offsetLg = offset.lg,
      offsetMd = offset.md,
      offsetSm = offset.sm,
      offsetXs = offset.xs;
  var eltClass = (0, _util.classNames)({
    blk: blk,
    className: className,
    mods: (_mods = {}, _defineProperty(_mods, "xs-".concat(xs), !!xs), _defineProperty(_mods, "sm-".concat(sm), !!sm), _defineProperty(_mods, "md-".concat(md), !!md), _defineProperty(_mods, "lg-".concat(lg), !!lg), _defineProperty(_mods, "xlg-".concat(xlg), !!xlg), _defineProperty(_mods, "xxlg-".concat(xxlg), !!xxlg), _defineProperty(_mods, "offset-xs-".concat(offsetXs), !!offsetXs), _defineProperty(_mods, "offset-md-".concat(offsetMd), !!offsetMd), _defineProperty(_mods, "offset-sm-".concat(offsetSm), !!offsetSm), _defineProperty(_mods, "offset-lg-".concat(offsetLg), !!offsetLg), _defineProperty(_mods, "offset-xlg-".concat(offsetXlg), !!offsetXlg), _defineProperty(_mods, "offset-xxlg-".concat(offsetXxlg), !!offsetXxlg), _mods)
  });
  return /*#__PURE__*/_react["default"].createElement(Tag, {
    className: eltClass,
    "data-auto-id": dataAutoId
  }, children);
};

Column.propTypes = {
  children: (0, _propTypes.oneOfType)([_propTypes.element, (0, _propTypes.arrayOf)(_propTypes.element)]).isRequired,
  col: (0, _propTypes.shape)({
    xs: _propTypes.number
  }).isRequired,
  offset: (0, _propTypes.shape)({
    xs: _propTypes.number
  }),
  className: _propTypes.string,
  dataAutoId: _propTypes.string,
  tag: _propTypes.string
};
Column.defaultProps = {
  offset: {},
  className: '',
  dataAutoId: 'column',
  tag: 'div'
};

var _default = /*#__PURE__*/(0, _react.memo)(Column);

exports["default"] = _default;
//# sourceMappingURL=index.js.map