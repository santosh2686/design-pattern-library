import React, { memo } from 'react'
import {
  arrayOf, bool, func, node, number, oneOfType, shape, string,
} from 'prop-types'

import { classNames } from '../../util'

import TabButton from '../tab-button'

import './_style.scss'

const blk = 'dls-tab-list'

const TabList = ({
  dataAutoId,
  className,
  data,
  fluid,
  selected,
  isRouted,
  selectHandler,
}) => {
  const eltClass = classNames({
    blk,
    className,
  })
  return (
    <div className={eltClass} data-auto-id={dataAutoId} role="tablist">
      {data.map(({ label, link, icon }, index) => {
        const key = `tab-${index}`
        return (
          <TabButton
            key={key}
            isRouted={isRouted}
            label={label}
            dataAutoId={key}
            icon={icon}
            selected={selected === index}
            link={link}
            fluid={fluid}
            selectHandler={() => selectHandler(index)}
          />
        )
      })}
    </div>
  )
}

TabList.propTypes = {
  data: arrayOf(shape({
    label: oneOfType([node, func]).isRequired,
    link: string,
  })).isRequired,
  selected: number,
  isRouted: bool,
  fluid: bool,
  selectHandler: func,
  className: string,
  dataAutoId: string,
}

TabList.defaultProps = {
  selected: 0,
  isRouted: false,
  fluid: false,
  selectHandler: () => {},
  className: '',
  dataAutoId: 'tabs',
}

export default memo(TabList)
