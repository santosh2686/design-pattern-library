"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = require("prop-types");

var _util = require("../../util");

var _step = _interopRequireDefault(require("./step"));

require("./_style.scss");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var blk = 'dls-stepper';

var Stepper = function Stepper(_ref) {
  var dataAutoId = _ref.dataAutoId,
      className = _ref.className,
      data = _ref.data,
      activeStep = _ref.activeStep;
  var eltClass = (0, _util.classNames)({
    blk: blk,
    className: className
  });
  return /*#__PURE__*/_react["default"].createElement("div", {
    "data-auto-id": dataAutoId,
    className: eltClass
  }, data.map(function (stepItem, index) {
    var key = "step-item-".concat(index);
    return /*#__PURE__*/_react["default"].createElement(_step["default"], {
      key: key,
      step: stepItem,
      activeStep: activeStep,
      stepIndex: index
    });
  }));
};

Stepper.propTypes = {
  dataAutoId: _propTypes.string,
  className: _propTypes.string,
  data: (0, _propTypes.arrayOf)((0, _propTypes.shape)({
    label: _propTypes.string,
    className: _propTypes.string
  })),
  activeStep: _propTypes.number
};
Stepper.defaultProps = {
  dataAutoId: 'stepper',
  className: '',
  data: [],
  activeStep: 1
};

var _default = /*#__PURE__*/(0, _react.memo)(Stepper);

exports["default"] = _default;
//# sourceMappingURL=index.js.map