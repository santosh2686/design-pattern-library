import isEmpty from '../is-empty'

const computeValue = (obj, map) => !isEmpty(map) && map.split('.').reduce((acc, next) => ((acc && !isEmpty(acc[next])) ? acc[next] : ''), obj)

export default computeValue
