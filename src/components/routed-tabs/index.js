import React, { memo, Fragment } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import {
  arrayOf, bool, shape, func, string, number,
} from 'prop-types'

import { classNames } from '../../util'

import TabList from '../tab-list'

import './_style.scss'

const blk = 'dls-routed-tabs'

const RoutedTabs = ({
  dataAutoId,
  data,
  fluid,
  defaultSelected,
  className,
  contentClassName,
}) => {
  const contentClass = classNames({
    blk,
    elt: 'content',
    className: contentClassName,
  })
  return (
    <>
      <TabList
        isRouted
        data={data}
        fluid={fluid}
        className={className}
        dataAutoId={dataAutoId}
      />
      <div
        className={contentClass}
      >
        <Switch>
          {data.map((tab, index) => {
            const key = `tabComponent_${index}`
            const { route, exact, component } = tab
            const TabComponent = component
            return (
              <Route key={key} exact={exact} path={route} component={TabComponent} />
            )
          })}
          <Route render={() => <Redirect to={data[defaultSelected].link} />} />
        </Switch>
      </div>
    </>
  )
}

RoutedTabs.propTypes = {
  data: arrayOf(shape({
    link: string.isRequired,
    route: string.isRequired,
    component: func.isRequired,
  })).isRequired,
  fluid: bool,
  className: string,
  contentClassName: string,
  dataAutoId: string,
  defaultSelected: number,
}

RoutedTabs.defaultProps = {
  fluid: true,
  className: '',
  contentClassName: '',
  dataAutoId: '',
  defaultSelected: 0,
}

export default memo(RoutedTabs)
