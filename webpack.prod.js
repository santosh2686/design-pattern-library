const path = require('path');

module.exports = {
  entry: {
    button: './src/components/button/index.js'
  },
  externals: [{
    react: 'react',
    'prop-types': 'prop-types',
    webpack: 'webpack'
  }],
  mode: 'production',
  output: {
    path: path.resolve('build'),
    publicPath: '/',
    filename: '[name]/[name].js',
    sourceMapFilename: '[name]/[name].js.map'
  },
  resolve: {
    extensions: ['.jsx', '.js', '.json'],
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      }
    ]
  },
  plugins: []
};
