import React, { memo } from 'react'
import { number, string, oneOfType } from 'prop-types'

import { classNames } from '../../util'

import './_style.scss'

const blk = 'dls-badge'

const Badge = ({ dataAutoId, className, value }) => {
  const eltClass = classNames({
    blk,
    className,
  })

  return (
    <div
      className={eltClass}
      data-auto-id={dataAutoId}
    >
      {value}
    </div>
  )
}

Badge.propTypes = {
  dataAutoId: string,
  className: string,
  value: oneOfType([string, number]).isRequired,
}

Badge.defaultProps = {
  dataAutoId: 'badge',
  className: '',
}

export default memo(Badge)
