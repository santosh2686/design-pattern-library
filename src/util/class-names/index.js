import bemClasses from 'css-bem-classes'

const classNames = ({
  blk = '', elt = '', mods = {}, className = '',
}) => {
  const cn = bemClasses(blk, { element: '__', modifier: '--' })
  let classString = cn(mods).trim()
  if (elt) {
    classString = cn(elt, mods).trim()
  }
  classString = className ? `${classString} ${className}` : classString
  return classString.trim()
}

export default classNames
