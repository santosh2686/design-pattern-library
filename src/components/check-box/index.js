import React, { PureComponent } from 'react'
import { bool, string, func } from 'prop-types'

import { classNames } from '../../util'

import './_style.scss'

const blk = 'dls-check-box'

class CheckBox extends PureComponent {
  handleChange = (event) => {
    const { target: { checked } } = event
    const { name, changeHandler } = this.props
    changeHandler({ [name]: checked })
  };

  render() {
    const {
      dataAutoId,
      id,
      className,
      name,
      disabled,
      checked,
      label,
    } = this.props
    const eltClass = classNames({ blk, className })
    const controlClass = classNames({ blk, elt: 'input' })
    const labelClass = classNames({ blk, elt: 'label' })
    return (
      <div
        data-auto-id={dataAutoId}
        className={eltClass}
      >
        <input
          type="checkbox"
          id={id}
          name={name}
          disabled={disabled}
          checked={checked}
          className={controlClass}
          onChange={this.handleChange}
        />
        <label htmlFor={id} className={labelClass}>
          {label}
        </label>
      </div>
    )
  }
}

CheckBox.propTypes = {
  label: string,
  dataAutoId: string,
  id: string.isRequired,
  name: string.isRequired,
  className: string,
  disabled: bool,
  checked: bool,
  changeHandler: func,
}

CheckBox.defaultProps = {
  label: '',
  dataAutoId: 'checkbox',
  className: '',
  disabled: false,
  checked: false,
  changeHandler: () => {},
}

export default CheckBox
