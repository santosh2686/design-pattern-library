import React, { memo } from 'react'
import { string, node } from 'prop-types'

import { classNames } from '../../../util'

import './_style.scss'

const blk = 'dls-header-cell'

const HeaderCell = ({ title, className }) => {
  const eltClass = classNames({
    blk,
    className,
  })
  return (
    <th className={eltClass}>
      {title}
    </th>
  )
}

HeaderCell.propTypes = {
  title: node.isRequired,
  className: string,
}

HeaderCell.defaultProps = {
  className: '',
}

export default memo(HeaderCell)
