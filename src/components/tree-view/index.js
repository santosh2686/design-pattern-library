import React, { memo } from 'react'
import {
  arrayOf, bool, string, node, shape,
} from 'prop-types'

import { classNames } from '../../util'

import ExpandableMenuItem from './expandable-menu-item'

import './_style.scss'

const blk = 'dls-tree-view'

const TreeView = ({
  dataAutoId,
  className,
  data,
  isAllExpanded,
}) => {
  const eltClass = classNames({
    blk,
    className,
  })
  return (
    <div data-auto-id={dataAutoId} className={eltClass}>
      <ExpandableMenuItem data={data} />
    </div>
  )
}

TreeView.propTypes = {
  className: string,
  dataAutoId: string,
  data: arrayOf(shape({
    label: node,
  })).isRequired,
  isAllExpanded: bool,
}

TreeView.defaultProps = {
  className: '',
  dataAutoId: 'treeView',
  isAllExpanded: false,
}

export default memo(TreeView)
