"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactRouterDom = require("react-router-dom");

var _propTypes = require("prop-types");

var _util = require("../../../util");

var _icon = _interopRequireDefault(require("../../icon"));

var _plus = _interopRequireDefault(require("../icon/plus.svg"));

var _minus = _interopRequireDefault(require("../icon/minus.svg"));

var _disable = _interopRequireDefault(require("../icon/disable.svg"));

require("./_style.scss");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var blk = 'dls-menu-item';

var MenuItem = function MenuItem(_ref) {
  var item = _ref.item,
      withChildren = _ref.withChildren,
      children = _ref.children;
  var label = item.label,
      _item$route = item.route,
      route = _item$route === void 0 ? '/' : _item$route,
      _item$expanded = item.expanded,
      expanded = _item$expanded === void 0 ? false : _item$expanded;

  var _useState = (0, _react.useState)(expanded),
      _useState2 = _slicedToArray(_useState, 2),
      isExpanded = _useState2[0],
      toggleExpand = _useState2[1];

  var subMenuClass = (0, _util.classNames)({
    blk: blk,
    elt: 'sub-menu',
    mods: {
      expanded: isExpanded
    }
  });

  if (withChildren) {
    return /*#__PURE__*/_react["default"].createElement(_react["default"].Fragment, null, /*#__PURE__*/_react["default"].createElement("button", {
      type: "button",
      className: (0, _util.classNames)({
        blk: blk,
        elt: 'button'
      }),
      onClick: function onClick() {
        return toggleExpand(!isExpanded);
      }
    }, /*#__PURE__*/_react["default"].createElement(_icon["default"], {
      className: (0, _util.classNames)({
        blk: blk,
        elt: 'icon'
      }),
      icon: isExpanded ? _minus["default"] : _plus["default"],
      isCustomSize: true,
      isCustomColor: true
    }), /*#__PURE__*/_react["default"].createElement("span", {
      className: (0, _util.classNames)({
        blk: blk,
        elt: 'label'
      })
    }, label)), /*#__PURE__*/_react["default"].createElement("div", {
      className: subMenuClass
    }, children));
  }

  return /*#__PURE__*/_react["default"].createElement(_reactRouterDom.NavLink, {
    to: route,
    exact: true,
    className: (0, _util.classNames)({
      blk: blk,
      elt: 'link'
    })
  }, /*#__PURE__*/_react["default"].createElement(_icon["default"], {
    icon: _disable["default"],
    className: (0, _util.classNames)({
      blk: blk,
      elt: 'icon',
      mods: {
        'link-icon': true
      }
    }),
    isCustomSize: true,
    isCustomColor: true
  }), /*#__PURE__*/_react["default"].createElement("span", {
    className: (0, _util.classNames)({
      blk: blk,
      elt: 'label'
    })
  }, label));
};

MenuItem.propTypes = {
  item: (0, _propTypes.shape)({
    label: _propTypes.string,
    route: _propTypes.string
  }).isRequired,
  withChildren: _propTypes.bool,
  children: _propTypes.node
};
MenuItem.defaultProps = {
  children: null,
  withChildren: false
};

var _default = /*#__PURE__*/(0, _react.memo)(MenuItem);

exports["default"] = _default;
//# sourceMappingURL=index.js.map