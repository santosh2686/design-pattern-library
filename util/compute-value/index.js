"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _isEmpty = _interopRequireDefault(require("../is-empty"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var computeValue = function computeValue(obj, map) {
  return !(0, _isEmpty["default"])(map) && map.split('.').reduce(function (acc, next) {
    return acc && !(0, _isEmpty["default"])(acc[next]) ? acc[next] : '';
  }, obj);
};

var _default = computeValue;
exports["default"] = _default;
//# sourceMappingURL=index.js.map