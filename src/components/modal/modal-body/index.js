import React, { memo } from 'react'
import { node, string } from 'prop-types'

import { classNames } from '../../../util'

import './_style.scss'

const blk = 'dls-modal-body'

const ModalBody = ({
  children,
  className,
}) => {
  const eltClass = classNames({
    blk,
    className,
  })
  return (
    <div className={eltClass}>
      {children}
    </div>
  )
}

ModalBody.propTypes = {
  children: node,
  className: string,
}

ModalBody.defaultProps = {
  children: '',
  className: '',
}

export default memo(ModalBody)
