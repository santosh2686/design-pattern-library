import React, { memo } from 'react'
import { func, string } from 'prop-types'

import { classNames } from '../../util'

import Icon from '../icon'

import closeIcon from './dls-close.svg'

import './_style.scss'

const blk = 'dls-close-button'

const CloseButton = ({ dataAutoId, className, clickHandler }) => {
  const eltClass = classNames({
    blk,
    className,
  })

  return (
    <button
      type="button"
      data-auto-id={dataAutoId}
      onClick={clickHandler}
      className={eltClass}
    >
      <Icon
        icon={closeIcon}
        isCustomSize
        isCustomColor
      />
    </button>
  )
}

CloseButton.propTypes = {
  dataAutoId: string,
  className: string,
  clickHandler: func,
}

CloseButton.defaultProps = {
  dataAutoId: '',
  className: '',
  clickHandler: () => {},
}

export default memo(CloseButton)
