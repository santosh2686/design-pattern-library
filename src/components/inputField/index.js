import React, { PureComponent } from 'react'
import {
  bool, string, oneOf, shape, number, func,
} from 'prop-types'

import { classNames } from '../../util'

import './_style.scss'

const blk = 'dls-input-field'

class InputField extends PureComponent {
  changeHandler = (event) => {
    const { target: { value } } = event
    const { name, changeHandler } = this.props
    changeHandler({
      [name]: value,
    })
  }

  render() {
    const {
      dataAutoId,
      id,
      label,
      value,
      name,
      type,
      placeholder,
      isRequired,
      state,
      maxLength,
      disabled,
      attributes,
      className,
    } = this.props
    const labelClasses = classNames({
      blk,
      elt: 'label',
      mods: {
        required: isRequired,
      },
    })
    const controlClasses = classNames({
      blk,
      elt: 'input',
      className,
      mods: {
        [state]: !!state,
      },
    })
    return (
      <div className={blk}>
        <label className={labelClasses}>
          {label}
        </label>
        <input
          id={id}
          type={type}
          name={name}
          value={value}
          className={controlClasses}
          disabled={disabled}
          maxLength={maxLength}
          data-auto-id={dataAutoId}
          placeholder={placeholder}
          onChange={this.changeHandler}
          // eslint-disable-next-line react/jsx-props-no-spreading
          {...attributes}
        />
      </div>
    )
  }
}

InputField.propTypes = {
  dataAutoId: string,
  id: string,
  label: string,
  value: string,
  name: string,
  placeholder: string,
  isRequired: bool,
  state: oneOf(['valid', 'invalid', '']),
  type: string,
  message: string,
  icon: string,
  labelInfo: string,
  showCharCount: bool,
  maxLength: number,
  changeHandler: func.isRequired,
  disabled: bool,
  className: string,
  controlClassName: string,
  isTextArea: bool,
  attributes: shape({}),
}

InputField.defaultProps = {
  dataAutoId: '',
  id: '',
  label: '',
  value: '',
  name: '',
  type: 'text',
  placeholder: '',
  isRequired: false,
  state: '',
  message: '',
  icon: '',
  labelInfo: '',
  showCharCount: false,
  maxLength: 80,
  disabled: false,
  className: '',
  controlClassName: '',
  isTextArea: false,
  attributes: {},
}

export default InputField
