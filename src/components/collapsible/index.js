import React, { memo } from 'react'
import {
  bool, oneOfType, string, node,
} from 'prop-types'

import { classNames } from '../../util'

import './_style.scss'

const blk = 'dls-collapsible'

const Collapsible = ({
  dataAutoId,
  className,
  headerClassName,
  containerClassName,
  title,
  actionElement,
  children,
  isExpanded,
}) => {
  const eltClass = classNames({
    blk,
    className,
  })

  const headerClass = classNames({
    blk,
    elt: 'header',
    className: headerClassName,
  })

  const containerClass = classNames({
    blk,
    elt: 'container',
    className: containerClassName,
  })

  return (
    <div
      className={eltClass}
      data-auto-id={dataAutoId}
    >
      <div className={headerClass}>
        <div className={classNames({ blk, elt: 'title' })}>
          {title}
        </div>
        <div>
          {actionElement}
        </div>
      </div>
      {isExpanded && (
        <div className={containerClass}>
          {children}
        </div>
      )}
    </div>
  )
}

Collapsible.propTypes = {
  dataAutoId: string,
  className: string,
  containerClassName: string,
  headerClassName: string,
  title: oneOfType([string, node]),
  actionElement: node,
  children: node.isRequired,
  isExpanded: bool,
}

Collapsible.defaultProps = {
  dataAutoId: 'collapsible',
  className: '',
  containerClassName: '',
  headerClassName: '',
  title: '',
  actionElement: null,
  isExpanded: true,
}

export default memo(Collapsible)
