import React, { memo } from 'react'
import { bool, string, node } from 'prop-types'

import { classNames } from '../../util'

import './_style.scss'

const blk = 'dls-icon'

const Icon = ({
  dataAutoId,
  className,
  icon,
  size,
  color,
  isCustomSize,
  isCustomColor,
}) => {
  const eltClass = classNames({
    blk,
    className,
    mods: {
      [size]: !!size && !isCustomSize,
      [color]: !!color && !isCustomColor,
      'custom-size': isCustomSize,
      'custom-color': isCustomColor,
    },
  })

  return (
    <span
      data-auto-id={dataAutoId}
      className={eltClass}
      dangerouslySetInnerHTML={{ __html: icon }}
    />
  )
}

Icon.propTypes = {
  dataAutoId: string,
  className: string,
  icon: node.isRequired,
  size: string,
  color: string,
  isCustomSize: bool,
  isCustomColor: bool,
}

Icon.defaultProps = {
  dataAutoId: '',
  className: '',
  size: 'default',
  color: 'default',
  isCustomSize: false,
  isCustomColor: false,
}

export default memo(Icon)
