import React, { memo } from 'react'
import { node, string } from 'prop-types'

import { classNames } from '../../util'

import './_style.scss'

const blk = 'dls-container'

const Container = ({
  children,
  className,
  dataAutoId,
  tag: Tag,
}) => {
  const eltClass = classNames({
    blk,
    className,
  })

  return (
    <Tag
      className={eltClass}
      data-auto-id={dataAutoId}
    >
      {children}
    </Tag>
  )
}

Container.propTypes = {
  children: node.isRequired,
  className: string,
  dataAutoId: string,
  tag: string,
}

Container.defaultProps = {
  className: '',
  dataAutoId: 'container',
  tag: 'main',
}

export default memo(Container)
