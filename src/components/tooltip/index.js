import React, { memo } from 'react'
import {
  bool, string, oneOf, node, func,
} from 'prop-types'

import { classNames } from '../../util'

import TooltipWrapper from './tooltip-wrapper'
import TooltipContent from './tooltip-content'

import './_style.scss'

const blk = 'dls-tooltip'

const ToolTip = ({
  dataAutoId,
  className,
  wrapperClass,
  contentClass,
  category,
  position,
  align,
  children,
  content,
  isClosable,
  closeButtonDataAutoId,
  closeHandler,
}) => {
  const eltClass = classNames({
    blk,
    wrapperClass,
  })

  return (
    <div className={eltClass} data-auto-id={dataAutoId}>
      {children}
      <TooltipWrapper
        className={className}
        position={position}
        align={align}
        category={category}
      >
        <TooltipContent
          className={contentClass}
          isClosable={isClosable}
          dataAutoId={closeButtonDataAutoId}
          closeHandler={closeHandler}
          category={category}
        >
          {content}
        </TooltipContent>
      </TooltipWrapper>
    </div>
  )
}

ToolTip.propTypes = {
  dataAutoId: string,
  className: string,
  wrapperClass: string,
  contentClass: string,
  category: string,
  position: oneOf(['top', 'bottom']),
  align: oneOf(['left', 'center', 'right']),
  children: node.isRequired,
  content: node.isRequired,
  closeHandler: func,
  closeButtonDataAutoId: string,
  isClosable: bool,
}

ToolTip.defaultProps = {
  dataAutoId: 'dlsTooltip',
  className: '',
  wrapperClass: '',
  contentClass: '',
  category: 'primary',
  position: 'top',
  align: 'center',
  closeHandler: () => {},
  closeButtonDataAutoId: 'btnClose',
  isClosable: false,
}

export default memo(ToolTip)
